## Arquivo de Pedido
gerador-layouts arquivoDePedido header IdentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 DataPedido:int32:12:20 CodigoClienteFornecedor:string:20:30 CnpjCliente:string:30:44 CnpjFornecedor:string:44:58 DiasVencimentoPrimeiraParcela:int32:58:61 DiasVencimentoSegundaParcela:int32:61:64 DiasVencimentoTerceiraParcela:int32:64:67 DiasVencimentoQuartaParcela:int32:67:70 DiasVencimentoQuintaParcela:int32:70:73 DiasVencimentoSextaParcela:int32:73:76 DiasVencimentoSetimaParcela:int32:76:79 DiasVencimentoOitavaParcela:int32:79:82 DiasVencimentoNonaParcela:int32:82:85 DiasVencimentoDecimaParcela:int32:85:88

gerador-layouts arquivoDePedido itens IndentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 CodigoProdutoFornecedor:string:12:22 CodigoBarrasProduto:int64:22:36 CodigoProdutoCliente:int64:36:46 Quantidade:int32:46:54 PrecoCompra:float64:54:66:2

gerador-layouts arquivoDePedido trailler IdentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 QuantidadeRegistrosArquivo:int32:12:20 SomaQuantidades:int32:20:28


## Retorno de Pedido
gerador-layouts retornoDePedido header IdentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 CodigoClienteFornecedor:int64:12:22 CnpjCliente:string:22:36 CnpjFornecedor:string:36:50 DataProcessamento:int32:50:58 HoraProcessamento:int32:58:64 NumeroPedidoFornecedor:int64:64:74

gerador-layouts retornoDePedido itens IdentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 CodigoProdutoFornecedor:string:12:22 CodigoBarrasProduto:int64:22:36 QuantidadeSolicitada:int32:36:44 QuantidadeNaoAtendida:int32:44:52 DescricaoMotivoNaoAtendimento:string:52:92

gerador-layouts retornoDePedido trailler IdentificadorTipoRegistro:string:0:2 NumeroPedidoCliente:int64:2:12 QuantidadeRegistrosArquivo:int32:12:20


## Arquivo de Precos
gerador-layouts arquivoDePrecos header IdentificadorTipoRegistro:string:0:2 CnpjFornecedor:int64:2:16 DataGeracaoArquivo:int32:16:24 HoraGeracaoArquivo:int32:24:30

gerador-layouts arquivoDePrecos produtos IdentificadorTipoRegistro:string:0:2 CodigoProdutoFornecedor:int64:2:16 DataGeracaoArquivo:int32:16:24 HoraGeracaoArquivo:int32:24:30

gerador-layouts arquivoDePrecos trailler IdentificadorTipoRegistro:string:0:2 QuantidadeRegistrosArquivo:int32:2:10