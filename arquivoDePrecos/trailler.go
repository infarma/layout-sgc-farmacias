package arquivoDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	IdentificadorTipoRegistro  string `json:"IdentificadorTipoRegistro"`
	QuantidadeRegistrosArquivo int32  `json:"QuantidadeRegistrosArquivo"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeRegistrosArquivo, "QuantidadeRegistrosArquivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro":  {0, 2, 0},
	"QuantidadeRegistrosArquivo": {2, 10, 0},
}
