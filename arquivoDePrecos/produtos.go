package arquivoDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	CodigoProdutoFornecedor   int64  `json:"CodigoProdutoFornecedor"`
	DataGeracaoArquivo        int32  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo        int32  `json:"HoraGeracaoArquivo"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DataGeracaoArquivo, "DataGeracaoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.HoraGeracaoArquivo, "HoraGeracaoArquivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro": {0, 2, 0},
	"CodigoProdutoFornecedor":   {2, 16, 0},
	"DataGeracaoArquivo":        {16, 24, 0},
	"HoraGeracaoArquivo":        {24, 30, 0},
}
