package arquivoDePrecos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	CnpjFornecedor            int64  `json:"CnpjFornecedor"`
	DataGeracaoArquivo        int32  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo        int32  `json:"HoraGeracaoArquivo"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataGeracaoArquivo, "DataGeracaoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraGeracaoArquivo, "HoraGeracaoArquivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro": {0, 2, 0},
	"CnpjFornecedor":            {2, 16, 0},
	"DataGeracaoArquivo":        {16, 24, 0},
	"HoraGeracaoArquivo":        {24, 30, 0},
}
