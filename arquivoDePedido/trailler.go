package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	IdentificadorTipoRegistro  string `json:"IdentificadorTipoRegistro"`
	NumeroPedidoCliente        int64  `json:"NumeroPedidoCliente"`
	QuantidadeRegistrosArquivo int32  `json:"QuantidadeRegistrosArquivo"`
	SomaQuantidades            int32  `json:"SomaQuantidades"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeRegistrosArquivo, "QuantidadeRegistrosArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.SomaQuantidades, "SomaQuantidades")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro":  {0, 2, 0},
	"NumeroPedidoCliente":        {2, 12, 0},
	"QuantidadeRegistrosArquivo": {12, 20, 0},
	"SomaQuantidades":            {20, 28, 0},
}
