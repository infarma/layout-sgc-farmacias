package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Header   Header   `json:"Header"`
	Itens    []Itens  `json:"Itens"`
	Trailler Trailler `json:"Trailler"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var index int32
		if identificador == "P1" {
			err = arquivo.Header.ComposeStruct(string(runes))
		} else if identificador == "P2" {
			err = arquivo.Itens[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "P9" {
			err = arquivo.Trailler.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
