package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	IndentificadorTipoRegistro string  `json:"IndentificadorTipoRegistro"`
	NumeroPedidoCliente        int64   `json:"NumeroPedidoCliente"`
	CodigoProdutoFornecedor    string  `json:"CodigoProdutoFornecedor"`
	CodigoBarrasProduto        int64   `json:"CodigoBarrasProduto"`
	CodigoProdutoCliente       int64   `json:"CodigoProdutoCliente"`
	Quantidade                 int32   `json:"Quantidade"`
	PrecoCompra                float64 `json:"PrecoCompra"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.IndentificadorTipoRegistro, "IndentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoBarrasProduto, "CodigoBarrasProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProdutoCliente, "CodigoProdutoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoCompra, "PrecoCompra")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"IndentificadorTipoRegistro": {0, 2, 0},
	"NumeroPedidoCliente":        {2, 12, 0},
	"CodigoProdutoFornecedor":    {12, 22, 0},
	"CodigoBarrasProduto":        {22, 36, 0},
	"CodigoProdutoCliente":       {36, 46, 0},
	"Quantidade":                 {46, 54, 0},
	"PrecoCompra":                {54, 66, 2},
}
