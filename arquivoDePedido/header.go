package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	IdentificadorTipoRegistro     string `json:"IdentificadorTipoRegistro"`
	NumeroPedidoCliente           int64  `json:"NumeroPedidoCliente"`
	DataPedido                    int32  `json:"DataPedido"`
	CodigoClienteFornecedor       string `json:"CodigoClienteFornecedor"`
	CnpjCliente                   string `json:"CnpjCliente"`
	CnpjFornecedor                string `json:"CnpjFornecedor"`
	DiasVencimentoPrimeiraParcela int32  `json:"DiasVencimentoPrimeiraParcela"`
	DiasVencimentoSegundaParcela  int32  `json:"DiasVencimentoSegundaParcela"`
	DiasVencimentoTerceiraParcela int32  `json:"DiasVencimentoTerceiraParcela"`
	DiasVencimentoQuartaParcela   int32  `json:"DiasVencimentoQuartaParcela"`
	DiasVencimentoQuintaParcela   int32  `json:"DiasVencimentoQuintaParcela"`
	DiasVencimentoSextaParcela    int32  `json:"DiasVencimentoSextaParcela"`
	DiasVencimentoSetimaParcela   int32  `json:"DiasVencimentoSetimaParcela"`
	DiasVencimentoOitavaParcela   int32  `json:"DiasVencimentoOitavaParcela"`
	DiasVencimentoNonaParcela     int32  `json:"DiasVencimentoNonaParcela"`
	DiasVencimentoDecimaParcela   int32  `json:"DiasVencimentoDecimaParcela"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoClienteFornecedor, "CodigoClienteFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoPrimeiraParcela, "DiasVencimentoPrimeiraParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoSegundaParcela, "DiasVencimentoSegundaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoTerceiraParcela, "DiasVencimentoTerceiraParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoQuartaParcela, "DiasVencimentoQuartaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoQuintaParcela, "DiasVencimentoQuintaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoSextaParcela, "DiasVencimentoSextaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoSetimaParcela, "DiasVencimentoSetimaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoOitavaParcela, "DiasVencimentoOitavaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoNonaParcela, "DiasVencimentoNonaParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DiasVencimentoDecimaParcela, "DiasVencimentoDecimaParcela")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro":     {0, 2, 0},
	"NumeroPedidoCliente":           {2, 12, 0},
	"DataPedido":                    {12, 20, 0},
	"CodigoClienteFornecedor":       {20, 30, 0},
	"CnpjCliente":                   {30, 44, 0},
	"CnpjFornecedor":                {44, 58, 0},
	"DiasVencimentoPrimeiraParcela": {58, 61, 0},
	"DiasVencimentoSegundaParcela":  {61, 64, 0},
	"DiasVencimentoTerceiraParcela": {64, 67, 0},
	"DiasVencimentoQuartaParcela":   {67, 70, 0},
	"DiasVencimentoQuintaParcela":   {70, 73, 0},
	"DiasVencimentoSextaParcela":    {73, 76, 0},
	"DiasVencimentoSetimaParcela":   {76, 79, 0},
	"DiasVencimentoOitavaParcela":   {79, 82, 0},
	"DiasVencimentoNonaParcela":     {82, 85, 0},
	"DiasVencimentoDecimaParcela":   {85, 88, 0},
}
