package layout_sgc_farmacias

import (
"bitbucket.org/infarma/layout-sgc-farmacias/arquivoDePedido"
"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}
