package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	IdentificadorTipoRegistro string `json:"IdentificadorTipoRegistro"`
	NumeroPedidoCliente       int64  `json:"NumeroPedidoCliente"`
	CodigoClienteFornecedor   int64  `json:"CodigoClienteFornecedor"`
	CnpjCliente               string `json:"CnpjCliente"`
	CnpjFornecedor            string `json:"CnpjFornecedor"`
	DataProcessamento         int32  `json:"DataProcessamento"`
	HoraProcessamento         int32  `json:"HoraProcessamento"`
	NumeroPedidoFornecedor    int64  `json:"NumeroPedidoFornecedor"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoClienteFornecedor, "CodigoClienteFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataProcessamento, "DataProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraProcessamento, "HoraProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoFornecedor, "NumeroPedidoFornecedor")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro": {0, 2, 0},
	"NumeroPedidoCliente":       {2, 12, 0},
	"CodigoClienteFornecedor":   {12, 22, 0},
	"CnpjCliente":               {22, 36, 0},
	"CnpjFornecedor":            {36, 50, 0},
	"DataProcessamento":         {50, 58, 0},
	"HoraProcessamento":         {58, 64, 0},
	"NumeroPedidoFornecedor":    {64, 74, 0},
}
