package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	IdentificadorTipoRegistro     string `json:"IdentificadorTipoRegistro"`
	NumeroPedidoCliente           int64  `json:"NumeroPedidoCliente"`
	CodigoProdutoFornecedor       string `json:"CodigoProdutoFornecedor"`
	CodigoBarrasProduto           int64  `json:"CodigoBarrasProduto"`
	QuantidadeSolicitada          int32  `json:"QuantidadeSolicitada"`
	QuantidadeNaoAtendida         int32  `json:"QuantidadeNaoAtendida"`
	DescricaoMotivoNaoAtendimento string `json:"DescricaoMotivoNaoAtendimento"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.IdentificadorTipoRegistro, "IdentificadorTipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoBarrasProduto, "CodigoBarrasProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeSolicitada, "QuantidadeSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescricaoMotivoNaoAtendimento, "DescricaoMotivoNaoAtendimento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"IdentificadorTipoRegistro":     {0, 2, 0},
	"NumeroPedidoCliente":           {2, 12, 0},
	"CodigoProdutoFornecedor":       {12, 22, 0},
	"CodigoBarrasProduto":           {22, 36, 0},
	"QuantidadeSolicitada":          {36, 44, 0},
	"QuantidadeNaoAtendida":         {44, 52, 0},
	"DescricaoMotivoNaoAtendimento": {52, 92, 0},
}
